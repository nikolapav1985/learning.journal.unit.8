Learning journal unit 8
-----------------------

- dctf.py (dictionary example, read from file, print to file)
- data (test data used to populate dictionary)
- ./dctf.py < data > data.inv (test a script)

Test environment
----------------

os lubuntu 16.04 lts 4.13.0 kernel version 2.7.12 python version
