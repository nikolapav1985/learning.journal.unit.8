#!/usr/bin/python

"""
FILE dctf.py

dictionary examples

RUN SCRIPT EXAMPLE

./dctf.py < data > data.inv

INPUT FILE EXAMPLE

1
1
2
4
3
9
4
16
5
25
6
36
7
49
8
64
9
81

OUTPUT FILE EXAMPLE

25
5
49
7
16
4
36
6
1
1
64
8
4
2
9
3
81
9

COMMENTS

original dictionary items are given one per line in a file. couple of lines are read at the time. first line is dictionary key and second line is item to be stored for given key. inverted dictionary items are stored one per line in a file too. for every couple of lines, first line is a key and second line is a value. in current example dictionary is treated as a function f(x)=x^2 (and inverse is g(x)=sqrt(x)). for each pair of lines in a file first entry is a number and second entry is square of that number (or square root in case of inverse).
"""

import sys

def invertdict(dct,inv):
    """
        method invertdict

        invert a dictionary (use values as keys)

        parameters

        dct(dictionary) a dictionary to use
        inv(dictionary) an inverted dictionary
    """
    item = None

    for key in dct:
        item = dct[key]
        if item not in inv:
            inv[item]=key
        else:
            inv[item].append(key)

if __name__ == "__main__":
    items = []
    count = 0
    dct = dict()
    inv = dict()
    line = None

    for line in sys.stdin:
        """
            add items to dictionary
        """
        items.append(line.rstrip())
        count += 1
        if count%2 == 0:
            dct[items[0]]=items[1]
            del items[:]

    invertdict(dct,inv)

    for line in inv:
        print(line)
        print(inv[line])
